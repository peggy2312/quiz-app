# Quiz App

**UNNC GRP Project developed by team 202112**

TeamWebsite: Click [here](http://cslinux.nottingham.edu.cn/~Team202112/) (make sure your network is eduroam of UNNC)

The following sections include:

- [Quiz App Try Out](# Quiz App Try Out)
- [Quiz App Local Deployment](# Quiz App Local Deployment)

# Quiz App Try Out



Quiz App is a web application, click [here](http://https://www.quizapp202112.xyz/h5/) to try our product.

You will be directed to the log in page of our applicaion. There are options for registering as student or teacher. You can try both of the role by creating an account and log in. 

A teacher can ceate quiz and questions. There is also functions of viewing student answers.

A new student will have no quiz in the list. Quizzes can be added to list by searching a name. Only correct name will be accepted.

For more functions, you may refer to our [user manual].

# Quiz App Local Deployment

Before deploying you will be required to prepare the following things:

### HBuilder (Compulsory)

This tool is useful for developing an uni-app or vue.js based project. Click [here](https://www.dcloud.io/hbuilderx.html) to download.

### Uni-Cloud Account (Compulsory)

The [Uni-Cloud](https://unicloud.dcloud.net.cn/home) is used for remote server and database. Your account should has a space connected with [Ali cloud]() server. For quick access , you may contact the developer through [email](biyyj10@nottingham.edu.cn) to ask for an account for quick deployment. 

### Deployment

After the preparation, you should download the demo code and import in Hbuilder.

Initially, you will have to log in to Hbuilder and then go to the manifest.json file to get a new Appid. Click on the green button and in the following picture and the Appid will be generated automatically.

<img src="pic_md/ManifestConfiguration.jpeg" alt="ManifestConfiguration" style="zoom:50%;" />

After that, you will have to connect to a unicloud space. In the document directory, right click on the 'uniCloud', the second option means initializing cloud service space. Click on it and pick your server space to initiate cloud functions and database. This might take some time, and you can see that the unicloud controlling desk will update its progress. After this, you can have connected to database and server space.

<img src="pic_md/connectUnicloudSpace.png" alt="connectUnicloudSpace" style="zoom:50%;" />

After connecting, the project can be run by clicking on the top button and there are two options to run locally. The first section choosed in the following page is to run in Chrome and the other is to run in inbuilt-browser of Hbuilder. Both are accepted. Then the page will pop up.

<img src="pic_md/RunProject.png" alt="connectUnicloudSpace" style="zoom:50%;" />

The project builds a manager account automatically, the username is "manager" and the password is set to be "20215822". 













