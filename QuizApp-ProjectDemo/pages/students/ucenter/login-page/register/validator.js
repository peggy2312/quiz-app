export default {
	"username": {
		"rules": [{
				required: true,
				errorMessage: 'Please input your username!',
			},
			{
				minLength: 3,
				maxLength: 32,
				errorMessage: 'the length of usernameshould be between {minLength} and {maxLength}',
			},
			{
				validateFunction:function(rule,value,data,callback){
					console.log(value);
					if(/^1\d{10}$/.test(value) || /^(\w-*\.*)+@(\w-?)+(\.\w{2,})+$/.test(value)){
						callback('username should not be email or phone number')
					};
					return true
				}
			}
		],
		"label": "username"
	},
	"password":{
		"rules": [{
				required: true,
				errorMessage: 'password',
			},
			{
				minLength: 6,
				maxLength: 20,
				errorMessage: 'The length of password should be between {minLength} to {maxLength}',
			}
		],
		"label": "password"
	},
	"pwd2":{
		"rules": [{
		
				required: true,
				errorMessage: 'confirm password',
		
			},
			{
				minLength: 6,
				maxLength: 20,
				errorMessage: 'The length of password should be between {minLength} to {maxLength}',
			},
			{
				validateFunction:function(rule,value,data,callback){
					console.log(value);
					if(value!=data.password){
						callback('Password and confirm password are not similar')
					};
					return true
				}
			}
		],
		"label": "confirm password"
	}
}