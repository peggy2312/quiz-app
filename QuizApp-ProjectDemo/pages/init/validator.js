export default {
	"username": {
		"rules": [{
				required: true,
				errorMessage: 'Please input your username!',
			},
			{
				minLength: 3,
				maxLength: 32,
				errorMessage: 'length of the username should be between {minLength} to {maxLength}',
			},
			{
				validateFunction:function(rule,value,data,callback){
					console.log(value);
					if(/^1\d{10}$/.test(value) || /^(\w-*\.*)+@(\w-?)+(\.\w{2,})+$/.test(value)){
						callback('username cannot be phone number of email address')
					};
					return true
				}
			}
		],
		"label": "username"
	},
	"password":{
		"rules": [{
				required: true,
				errorMessage: 'Password',
			},
			{
				minLength: 6,
				maxLength: 20,
				errorMessage: 'length of the password should be between {minLength} to {maxLength}',
			}
		],
		"label": "password"
	},
	"pwd2":{
		"rules": [{
		
				required: true,
				errorMessage: 'confirm password',
		
			},{
				validateFunction:function(rule,value,data,callback){
					console.log(value);
					if(value!=data.password){
						callback('Input passwords are different!')
					};
					return true
				}
			}
		],
		"label": "confirmPassword"
	}
}