// need modification: change or delete links in sidebar
export default {
	login: {
		url: '/pages/login/login' // Log in route
	},
	index: {
		url: '/pages/index/index' // The initial page after logging in
	},
	error: {
		url: '/pages/error/404' // 404 Not Found error page
	},
	navBar: { // Navigation bar on top og the page
		logo: '/static/logo.png', // Logo on left top corner
		// links: [{ // links
		// 	text: 'To be Added/Delete',
		// 	url: 'https://uniapp.dcloud.net.cn/uniCloud/admin'
		// }, {
		// 	text: 'To be Added/Deleted',
		// 	url: 'https://ext.dcloud.net.cn/?cat1=7&cat2=74'
		// }],
		debug: {
			enable: process.env.NODE_ENV !== 'production', //If the error message will be shown
			engine: [{ // error navigation（each error message popped up will generate a link that jumps to other page）
				name: '百度',
				url: 'https://www.baidu.com/baidu?wd=ERR_MSG'
			}, {
				name: '谷歌',
				url: 'https://www.google.com/search?q=ERR_MSG'
			}]
		}
	},
	sideBar: { // left menu bar
		// 配置静态菜单列表（放置在用户被授权的菜单列表下边）
		// staticMenu: [{
		// 	menu_id: "demo",
		// 	text: 'to be changed',
		// 	icon: 'uni-icons-list',
		// 	url: "",
		// 	children: [{
		// 		menu_id: "icons",
		// 		text: 'to be changed',
		// 		icon: 'uni-icons-star',
		// 		value: '/pages/demo/icons/icons',
		// 	}, {
		// 		menu_id: "table",
		// 		text: 'to be changed',
		// 		icon: 'uni-icons-map',
		// 		value: '/pages/demo/table/table',
		// 	}]
		// }]
	}
}
