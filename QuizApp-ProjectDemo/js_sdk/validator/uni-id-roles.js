// 校验规则由 schema 生成，请不要直接修改当前文件，如果需要请在uniCloud控制台修改schema
// uniCloud: https://unicloud.dcloud.net.cn/



export default {
	"role_id": {
		"rules": [{
				"required": true,
				"errorMessage": "{label} is required"
			}, {
				"format": "string"
			}
		],
		"label": "Role Id"
	},
	"role_name": {
		"rules": [{
			"required": true,
			"errorMessage": "{label} is required"
		}, {
			"format": "string"
		}],
		"label": "role name"
	},
	"permission": {
		"rules": [{
			"format": "array"
		}],
		"label": "permissions"
	},
	"comment": {
		"rules": [{
			"format": "string"
		}],
		"label": "comment"
	}
}
