
// 表单校验规则由 schema2code 生成，不建议直接修改校验规则，而建议通过 schema2code 生成, 详情: https://uniapp.dcloud.net.cn/uniCloud/schema



const validator = {
  "questionnaire_id": {
    "rules": [
      {
        "required": true
      }
    ]
  },
  "title": {
    "rules": [
      {
        "required": true
      },
      {
        "format": "string"
      }
    ],
    "label": "question"
  },
  "description": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "description"
  },
  "type": {
    "rules": [
      {
        "required": true
      },
      {
        "format": "string"
      },
      {
        "range": [
          {
            "text": "single choice",
            "value": "radio"
          },
          {
            "text": "multi-choice",
            "value": "checkbox"
          },
          {
            "text": "fill in blank",
            "value": "input"
          },
          {
            "text": "sigle choice and other",
            "value": "radioinput"
          },
          {
            "text": "sigle choice and others",
            "value": "checkboxinput"
          }
        ]
      }
    ],
    "label": "option type"
  },
  "questionType": {
    "rules": [
      {
        "required": true
      },
      {
        "format": "string"
      },
      {
        "range": [
          {
            "text": "quiestionnaire",
            "value": "question"
          },
          {
            "text": "vote",
            "value": "vote"
          },
          {
            "text": "examination",
            "value": "examination"
          }
        ]
      }
    ],
    "label": "question type"
  },
  "is_required": {
    "rules": [
      {
        "format": "bool"
      }
    ],
    "label": "required"
  }
}

const enumConverter = {
  "type_valuetotext": {
    "radio": "Single Choice",
    "checkbox": "Multi-Choice",
    "input": "Fill-in-blank",
  },
  "questionType_valuetotext": {
    "question": "quiz",
    "vote": "vote",
    "examination": "examination"
  }
}
export { validator, enumConverter }
