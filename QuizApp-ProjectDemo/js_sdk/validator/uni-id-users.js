// 校验规则由 schema 生成，请不要直接修改当前文件，如果需要请在uniCloud控制台修改schema
// uniCloud: https://unicloud.dcloud.net.cn/



export default {
	"username": {
		"rules": [{
				"required": true,
				"errorMessage": "{label} is required"
			},
			{
				"format": "string"
			},
			{
				"minLength": 2
			}
		],
		"label": "用户名"
	},
	"password": {
		"rules": [{
				"required": true,
				"errorMessage": "{label} is required"
			},
			{
				"format": "string"
			},
			{
				"minLength": 6
			}
		],
		"label": "password"
	},
	"email": {
		"rules": [{
				"format": "string"
			},
			{
				"format": "email"
			}
		],
		"label": "email"
	},
	"role": {
		"rules": [{
			"format": "array"
		}]
	}
}
