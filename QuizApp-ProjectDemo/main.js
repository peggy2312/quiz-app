import Vue from 'vue'
import App from './App'
// import store from './store'


import store from './store/index.js';

import plugin from './js_sdk/uni-admin/plugin'
import openApp from './common/openApp.js';
Vue.config.productionTip = false

Vue.use(plugin)
openApp()
App.mpType = 'app'

const app = new Vue({
    store,
    ...App
})
app.$mount()
