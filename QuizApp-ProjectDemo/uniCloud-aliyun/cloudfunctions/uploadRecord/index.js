'use strict';

const db = uniCloud.database()
const dbCmd = db.command
const collection_search = db.collection('grp-student-quiz-history')

exports.main = async (event, context) => {
	//event is the parameters uploaded from client side
	
	// get the id of the quiz and add to the database with student id
	await collection_search.add({
		questionnaire_id: event.questionnaire_id,
		user_id: event.user_id
	})
	
	
	// return data to client side
	return {
		questionnaire_id: event.id,
		user_id: event.user_id
	}
};
