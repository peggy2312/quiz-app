'use strict';
const db = uniCloud.database();
const dbCmd = db.command
const aireCollection = db.collection('grp-vote-questionnaire')
const questionCollection = db.collection('grp-vote-question')
const optionCollection = db.collection('grp-vote-option')
const logCollection = db.collection('grp-vote-log')
let uniID = require('uni-id')
const createConfig = require('uni-config-center')
const uniIdConfig = createConfig({
	pluginId: 'uni-id'
}).config()
exports.main = async (event, context) => {
	//UNI_WYQ:这里的uniID换成新的，保证多人访问不会冲突
	uniID = uniID.createInstance({
		context
	})
	console.log('event : ' + JSON.stringify(event))
	const {
		operation,
		data,
		sysInfo,
		uniIdToken
	} = event;
	console.log('sysInfo : ' + JSON.stringify(sysInfo))
	console.log('uniIdToken : ', uniIdToken)
	try {
		switch (operation) {
			case "getAireList": {
				return await getAireList(data);
			}
			case "getAireContent": {
				return await getAireContent(data, uniIdToken, sysInfo.deviceId);
			}
			case "getAireContentResult": {
				return await getAireContentResult(data);
			}
			case "saveAire": {
				return await saveAire(data, uniIdToken, sysInfo.deviceId);
			}
			default: {
				throw new Error("未找到接口")
			}
		}
	} catch (e) {
		var msg = e.toString().replace("Error: ", "")
		console.log("Error!", msg)
		return {
			code: 50101,
			msg: msg
		};
	}

};
/**
 * 获取问卷列表
 */
async function getAireList(data) {
	const now = Date.now()
	var total = await aireCollection.where({
		deadline: dbCmd.gt(now)
	}).count();
	var res = await aireCollection.where({
			deadline: dbCmd.gt(now)
		})
		.orderBy('create_time', 'desc')
		.skip(data.skip)
		.limit(data.limit)
		.get();
	return {
		code: 200,
		data: {
			total: total.total,
			items: res.data
		}
	};
}
/**
 * 获取问卷结果
 */
async function getAireContentResult(data) {
	var questionList = await questionCollection.where({
			questionnaire_id: data.id
		})
		.get();
	for (var i = 0; i < questionList.data.length; i++) {
		var list = [];
		var optionList = await optionCollection.where({
				question_id: questionList.data[i]._id
			})
			.get();
		for (var j = 0; j < optionList.data.length; j++) {
			//获取票数
			const res = await db.collection('grp-vote-log')
				.where({
					"option_id": optionList.data[j]._id
				})
				.field({ 'device_id': true })
				//.distinct() // 注意distinct方法没有参数
				.get({
					getCount: true
				})
			list.push({
				value: res.data.length,
				text: optionList.data[j].title
			})
		}
		questionList.data[i].localdata = list;
	}
	return {
		code: 200,
		data: questionList.data
	};
}
/**
 * 获取问卷内容
 */
async function getAireContent(data, token, deviceId) {
	var aire = await aireCollection.where({
			_id: data.id
		})
		.get();
		console.log("searching:", data.id)
	if (aire.data.length == 0) {
		
		throw new Error("Quiz not found");
	}
	var payload = await uniID.checkToken(token);
	if (aire.data[0].anonymous != true && payload.code != 0) {
		return {
			code: 403,
			msg: "Non-anonymous Quiz"
		}
	}
	var questionList = await questionCollection.where({
			questionnaire_id: data.id
		})
		.get();
	for (var i = 0; i < questionList.data.length; i++) {
		var optionList = await optionCollection.where({
				question_id: questionList.data[i]._id
			})
			.get();
		questionList.data[i].optionList = optionList.data;
		//access answered options
		var log = await logCollection.where({
				question_id: questionList.data[i]._id,
				user_id: data.uid
			})
			.get();
		var select = "";
		var selectList = [];
		var other = "";
		if (log.data.length > 0) {
			switch (questionList.data[i].type) {
				case "radio":
					select = log.data[0].option_id[0];
					break;
				case "checkbox":
					selectList = log.data[0].option_id;
					break;
				case "input":
					other = log.data[0].option_id[0];
					break;
				case "radioinput":
					select = log.data[0].option_id[0];
					other = log.data[0].option_other;
					break;
				case "checkboxinput":
					selectList = log.data[0].option_id;
					other = log.data[0].option_other;
					break;
				default:
					break;
			}
		}
		questionList.data[i].select = select;
		questionList.data[i].selectList = selectList;
		questionList.data[i].other = other;
	}
	aire.data[0].questionList = questionList.data
	console.log("aire", aire);
	return {
		code: 200,
		data: aire.data[0]
	};
}
/**
 * 提交问卷
 */
async function saveAire(data, token, deviceId) {
	if (data.length == 0) {
		throw new Error("Please do no submit blank quiz");
	}
	var aire = await aireCollection.where({
			_id: data[0].questionnaire_id
		})
		.get();
	if (aire.data.length == 0) {
		throw new Error("Quiz not found!");
	}
	const now = Date.now();
	if (aire.data[0].deadline < now) {
		throw new Error("Quiz submission ended");
	}
	var payload = await uniID.checkToken(token);
	if (aire.data[0].anonymous != true && payload.code != 0) {
		return {
			code: 403,
			msg: "Non-anonymous Quiz"
		}
	}
	var userId = "";
	if (payload.code == 0) {
		userId = payload.uid;
	}
	for (var i = 0; i < data.length; i++) {
		var log = await logCollection.where({
				question_id: data[i].question_id,
				user_id: data[0].user_id
			})
			.get();
		if (log.data.length == 0) {
			data[i].create_time = now;
			data[i].device_id = deviceId;
			data[i].user_id = userId;
			var res = await logCollection.add(data[i]);
		} else {
			//判断是否可以编辑
			if (aire.data[0].canedit == false) {
				throw new Error("Only one attempt is allowed!")
			} else {
				await logCollection.doc(log.data[0]._id).update({
					option_id: data[i].option_id,
					is_correct: data[i].is_correct,
					create_time: now
				});
			}
		}
	}
	return {
		code: 200,
		data: "success"
	};
}
