'use strict';

const db = uniCloud.database()
const dbCmd = db.command
const collection_quiz_list = db.collection('grp-vote-questionnaire')

exports.main = async (event, context) => {
	//event is the data uploaded from user end (frontend)
	console.log('event1 : ', event)
	const res = await collection_quiz_list.where({
		title: dbCmd.eq(event.questionnaire_title)
	}).get();
	if (res.data.length == 0) {
		return {
			code: 20,
			msg: "Quiz not found!"
		}
	}
	

	return res.data[0]._id;


	//返回数据给客户端
	return event
};
