\chapter{Implementation}

This chapter mainly introduces the tools and languages used for implementation. Several concepts related will be clarified as well. Moreover, some essential implementing problems and core functions are illustrated.

\section{Previous Analysis on Implementation}


\subsection{Web App}

After the team analyzed the core requirements and did some research, we decided to develop the Quiz App as a web application. The following analysis has been conducted based on the feasibility and benefits of web applications.

Firstly, our application is expected to be \textbf{portable}, which means it could be run on all major platforms. In this case, web applications accessible from various platforms and various devices, for instance, laptops and mobile phones, should be a suitable choice for this application. In addition, web applications can be accessed through various browsers, which allows users with different operating systems like Windows, Mac and Linux  to access this application. 

Another expectation from the stakeholder is that Quiz App should be \textbf{lightweight}. Typically, web applications take up little hard disk space, compared with other forms of applications, which implies that running this application will not be over-demanding on the computational resources of the platform. Apparently, web applications fit the lightweight requirements. Apart from the reasons above, web applications have more benefits. They do not need to be updated because all new features are performed on the server and can be delivered to users automatically. This helps us incorporate various possible features into our Quip App in subsequent development.

\subsection{Programming Languages}

To create a web application, the team used \textbf{Uni-app\footnote{https://www.dcloud.io}} for front-end programming. This Vue.js-based framework is primarily used for front-end programming. It provides programmers with various customized components and templates, which cover plenty of required functions, relieving the team programmers from building the whole project from scratch. \\
In addition to front-end support, this framework also provides a cloud development platform called Uni-Cloud\footnote{https://uniapp.dcloud.io/uniCloud/}, which bases on a \textbf{serverless mode} in conjunction with Alibaba Cloud\footnote{https://www.aliyun.com} or Tencent Cloud\footnote{https://cloud.tencent.com}.  The platform enables us to complete the combined requirement of front-end and back-end development using javascript. The language consistency brings us convenience in achieving communication between the back-end and front-end.  Moreover, it naturally promotes the efficiency of code reviews and white-box testing.  In our black-box testing, the probability of misalignment of the front and back ends is greatly reduced, as well as the permission vulnerabilities. 

\subsection{Database}

Uni-app provides developers a back-end choice called \textbf{UniCloud database}, a document-type database in JSON format. This is a kind of non-relational open-source database called NoSQL\footnote{https://en.wikipedia.org/wiki/NoSQL}, which supports different records with various fields and implements multi-layer nested data.

This UniCloud database can be accessed by both cloud functions and directly on client-side. The former one utilizes JQL (Javascript Query Language)\footnote{https://uniapp.dcloud.io/uniCloud/jql.html#module}, which can help with master database queries and other more complex query operations. For client-side access, the operations are achieved by JQL grammar as well. However, it has a unique specification system called \textbf{DB schema} (\ref{Schema}) to ensure data security. In DB Schema, we need to configure data operation permissions and field domain checking rules to prevent inappropriate data reading and writing from front end. In addition to convenient access, Unicloud provides \textbf{opendb}\footnote{https://gitee.com/dcloud/opendb}, a set of open data table design specifications that includes a large number of open source database templates. With these templates, we can grasp database configuration swiftly. For templates used in this project see \ref{JSONfile}

\subsection{Server}

Due to the \textbf{serverless} feature of Uni-app, our team decided to rent computing and storage capabilities from cloud service providers, without buying new servers to cope with high concurrency and resist DDOS attacks. On account of the serverless feature, the cloud functions need a domain name to be deployed to the cloud, for which we purchased a domain name. Then the cloud functions were 
prepared and uploaded to uniCloud in the Alibaba Cloud version. Alibaba Cloud has built a huge serverless resource pool, and there are many node processes for the operation of cloud functions. When our app initiates a request, the serverless system could allocate idle resources to run our corresponding requests for cloud functions.

\section{Software and Tools}
1. Project management: Jira\footnote{	www.atlassian.com/software/jira}\\
2. Communication: Teams\footnote{https://teams.com/} and WeChat\footnote{https://web.wechat.com}\\
3. Dissertation writing: Typora\footnote{https://typora.io/} and Overleaf\footnote{https://www.overleaf.com} \\
4. Prototyping: Figma\footnote{https://www.figma.com/}\\
5. Diagrams: Visual Paradigm\footnote{https://www.visual-paradigm.com
} and EdrawMax\footnote{http://www.edrawsoft.com/}\\
6. Source code management: GitLab\footnote{https://gitlab.com}\\
7. Coding Editor: HBuilderX\footnote{https://www.dcloud.io/hbuilderx.html}

\section{System Overview}
\subsection{User Roles}

The first two roles are used by normal users, which are "teacher" and "student". There is an additional role called Manager ("admin"), who has access to all visible information displayed to both students and teachers. The manager has higher authority than anyone else and is a unique account for system management. \\
\textbf{Teacher}: \\
- Create, modify or delete quizzes and set quiz content, answer, deadline and other information.\\
- View all students' quiz attendance and answers.\\
- View the correctness of each question.\\

\textbf{Student}:\\
- Get a specific quiz with the given name.\\
- Take the quiz within its deadline and the specified number of times.\\
- View the result of the last attendance.\\


\textbf{Manager}: \\
- The role with the highest authority. \\
- View all the quizzes and existing users.\\
- Manage the permissions of the other two types of users\\
- Import the excel file to create accounts in batches according to the name list.\\


\subsection{Code Hierarchy}

The package can be generally divided into the following parts:
\\ 
- App.vue: the file for launching the application.
\\- pages.json: the file used to set global configuration. Set navigating route for each interface and style for the window and navigating bars.
\\- manifest.json: configuration file that sets the application name, icon and authorizations.
\\- package.json: this file enables releasing the project to different stages (WeChat, Web, etc.), by adding extension uni-app nodes to the file.
\\- uni.scss: defines the overall style of the project.
\\- components folder: the directory that contains the imported components with expected functions.
\\- pages folder: defines interfaces in released applications.
\\- static folder: stores the images and other static resources that are to be used in the interface.
\\- store folder: this file contians several js file that set js methods for storing local user information when a user successfully logs in. Local memory will be cleared as well when users log out. Local user information includes user name, device identification, user informational token, etc. The local information is set by utilizing Uni-app function: 
uni.setStorageSync('value name', value to be stored).
\\- windows folder: this folder contains the top and left bar that is static on every page. To hide them, it requires adding 

				\begin{centering}"topWindow": false,\\
				"leftWindow": false,
				
\\\end{centering}
 in "style column" of pages.json file.
 \begin{figure}[H]
  \centering
\includegraphics[width=0.8\linewidth]{Final Report/Implementation/image/code_hierarchy.png}
\caption{Code Hierarchy of Project}
	\label{fig:code_hierarchy.png}
\end{figure}

\subsection{Component and Uni-modules}
Both components and uni-modules provide a project with specific functionality. However, uni-modules is a modularized regularity of components, which is the encapsulation of js sdk, components, pages, uni-cloud function and public modules. Uni-modules can be individual projects as well as serve as components in projects. The cloud function in uni-modules will be virtually added to the uni-cloud function of the projects that import it. 
\subsubsection{Component}
1. \textbf{`download-excel'}: \\
    Set information to be exported and call function to export as excel file with set file name and column names.
    
2. \textbf{Menu components}:\\
   Several components are bound to one another, which compose the left menu bar.\\
   These menu-related components are used in the left menu bar. It uses the component called `uni-data-menu', inside which the selected color and other appearances are adjusted by using the component `uni-nav-menu'.\\
   Inside component `uni-nav-menu', it uses the component `uni-menu-sidebar', which displays the actual name of the menu and presents it to users with corresponding permissions using `uni-menu-item'.
   
3. \textbf{`fix-window'}:\\
   This is the overall fix component that will be fixed in all pages except for log-in and register page, which includes the left menu bar and top bar that displays the logo, name of users and so on. 
 

\subsubsection{Uni-modules}
1. \textbf{qiun-data-charts}\footnote{https://gitee.com/uCharts/uCharts}\\
    Generate a chart with given data to present data more perceptually. Quiz App uses it to generate a pie chart for questions with options to display the number of students selecting each option.\\
2.  \textbf{uni-card}\footnotemark{}\\
   Used in displaying full quiz to users, questions will be displayed as cards, with content, pictures and a place for answers. \\
3. \textbf{uni-forms}\footnotemark[\value{footnote}]\\
Used for students to do the quiz, all input answers will be saved through uni-forms and submitted to the specific database by calling corresponding cloud functions.\footnotetext{https://github.com/dcloudio/uni-ui}\\
4. \textbf{uni-id-cf}\footnote{https://ext.dcloud.net.cn/plugin?id=5057}\\
  This is the abbreviation of uni-id Cloud Function, this uni-module provides a verification uni-cloud function.

\subsection{Cloud Functions}
Cloud functions are javascript code that runs on the cloud, which is based on the extension of Node.js. Each cloud function is a js package, which can be used for multiple utilization. \\
Here we have used several function templates:

\begin{itemize}
   \item \textbf{uni-vote} \\There are four callable functions here:\\
1. \textbf{getAireList} is for the client-side to acquire the list of the quizzes.

2. \textbf{getAireContentResult} is for the client-side to acquire the content, answer and the correctness of the quiz attended by specified students. 

3. \textbf{getAireContent} is for the client-side to acquire the question list of the quizzes

4. \textbf{saveAire} is called when a student submits the quiz he attended, this function will save the answer to the database.

\item \textbf{uni-id-cf}

This function, which is responsible for the verification mechanism of registration, logging in and modifying the password, is called when the user logs in or registers for a new account. The function will check if the username matches the password. It returns the current local logged-in user information.

\item \textbf{common/uni-id\footnote{https://ext.dcloud.net.cn/plugin?id=2116}}

This function provides a series of verification rules for the registering user.

\item \textbf{getExcelToJson\footnote{https://ext.dcloud.net.cn/plugin?id=6626}}

This is an imported function in combination with a component that allows exporting data in the form of an excel file.

\item \textbf{find-quiz-id}

This is a customized cloud function that takes the input questionnaire title to search for the corresponding quiz id and adds it to a database together with the id of the user that sends search request. This is applied on the student side when a student wants to acquire a quiz through its name.

\end{itemize}


\subsection{Uni-Cloud Database}\label{JSONfile}
This app is based on uni-cloud database, which contains a large number of open-source database templates, and common data tables do not need to be designed by programmers.
There are several JSON format files in this project, whose suffixes are all ".schema.json". This file can be uploaded and implemented into a database on the service space that uni-cloud provides. There are officially provided\footnotemark[\value{footnote}] and self-defined JSON files.

\subsubsection{Officially Provided JSON files}

\footnotetext{https://gitee.com/dcloud/opendb/tree/master/collection}
-  \textbf{grp-vote-questionnaire}\\
This database is for storing the quizzes that teachers create. It is related to teachers by storing a field of teacher ID for each quiz. It also stores some information of the quizzes that are set by the teacher, such as deadlines, anonymous or not.\\
- \textbf{grp-vote-question}\\
The question database is for storing the questions in each quiz. The relation between questions and quizzes is built by storing a quiz ID in each record of question Database.\\
- \textbf{grp-vote-option}\\
The option database is used to store the options for single choice on multi-choice questions.\\
- \textbf{grp-vote-log}\\
This database will save the answers of a student attending the quiz. To do so, it will save the question ID, question ID and the answer, together with the student id.\\
- \textbf{uni-id-users}\\
This database is used for holding the username and corresponding password. It also stores the nickname and gender of the user. When a new user first registers, it will store the user information in this database. Whenever a user logs in, the program will check in this database if the username matches the corresponding password. \\
- \textbf{uni-id-log}\\
The database stores the user information of a specific account, including id address, device type and user id. The user id will be used to connect with the `uni-id-users'.\\
- \textbf{uni-id-permissions}\\
The unique manager of the APP can set the authorizations of other types of users. The set permission will be stored in this database.\\
- \textbf{opened-admin-menus}\\
This database contains all the menu bars that might be displayed on the left sidebar. The accessibility will be decided by the manager through setting the permission value.

\subsubsection{Self-defined JS files}
\textbf{grp-student-quiz-history}\\
    This is a self-define schema file, which is used when a student searches for a quiz, the queried quiz id will be stored in this database with the user id to keep a record of this match for long-term storage. With this match, students will always find this quiz in personal quiz table.
    
\section{Technical Issues}
\subsection{Mechanism of Register and Log In}
\subsubsection{Register Mechanism}
\textbf{Register for Teacher}\\
        The teacher registration is achieved in pages/demo/init.vue. The user is required to set the username and input the password twice to prevent mistakes. There is a validator that sets the rule for the username, checking whether the username is set appropriately before submitting the form and creating an account. Then it will call a cloud function, `uni-id-cf', specify the action name as `registerTeacher' and transfer the form data for registration.
    \begin{figure}[H]
    \subfigure[Rules for username and password]{
        \label{Fig.sub.1}
	    \includegraphics[scale=0.25]{Final Report/Implementation/image/teacherRegisterValidator.png}}
	 \hspace{1cm}
    \subfigure[Calling Cloud Function from Client-Side]{
            \label{Fig.sub.2}
	        \includegraphics[scale=0.4]{Final Report/Implementation/image/teacherRegisterCallFunction.png}}
	\label{fig:my_label}
	\caption{Front-end Work for Register}
	\end{figure}
	In Cloud function `uni-id-cf', the function accepts the action that is sent from front-end and finds the corresponding action case to do more verification and calls function `uni-id' to insert a new record of the user into database `uni-id-users'.
    \begin{figure}[H]
    \subfigure[Verification in uni-id-cf and call uni-id]{
        \label{Fig.sub.1}
	    \includegraphics[scale=0.35]{Final Report/Implementation/image/CloudFunctionRegisterTeacher.png}}
	 \hspace{1cm}
    \subfigure[Directory of uni-id-cf]{
            \label{Fig.sub.2}
	        \includegraphics[scale=0.35]{Final Report/Implementation/image/codeHerarchyIn_uni-id-cf.png}}
	\label{fig:my_label}
	\caption{Front-end Work for Register}
	\end{figure}
\textbf{Register for Students}\\
	The same process is executed for students, except that the role in `uni-id-cf' is set to be "student".
    \begin{figure}[H]
		\centering
		\includegraphics[width=0.4\linewidth]{Final Report/Implementation/image/CloudFunctionStudentRegister.png} 
	        \caption{Case Student in uni-id-cf}
	        \label{fig:my_label}
	    \end{figure}
	    \item \textbf{Multiple account creation}
	    There is another way for creating multiple student accounts at the same time. See \textbf{\ref{multiAccount}}
\subsubsection{Password Security}
To ensure a secured password, the password will go through a hashing procedure when sent to the database.
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.4\linewidth]{Final Report/Implementation/image/Password Hashing.png} 
	        \caption{Password Hashing Procedure}
	        \label{fig:my_label}
	\end{figure}
After this procedure, the password in the uni-cloud database is a string of hashed code.
	    \begin{figure}[H]
		\centering
		\includegraphics[width=0.7\linewidth]{Final Report/Implementation/image/userRecord.png}
	        \caption{Password in Database}
	        \label{fig:my_label}
	    \end{figure}
\subsubsection{Log in Mechanism}
To log in, when the users do the required actions and click on `log in', form data will be accessed and transferred to `uni-id-cf' function.  

    \begin{figure}[H]
    \subfigure[]{
        \label{Fig.sub.1}
	    \includegraphics[scale=0.35]{Final Report/Implementation/image/loginCallFunction1.png}}
    \subfigure[]{
        \label{Fig.sub.2}
	    \includegraphics[scale=0.35]{Final Report/Implementation/image/loginCallFunction2.png}}
	\label{fig:my_label}
	\caption{Call uni-id-cf from Front-end}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.5\linewidth]{Final Report/Implementation/image/CloudFunctionLogin.png} 
	        \caption{Case login in uni-id-cf}
	        \label{fig:my_label}
	\end{figure}
\subsection{Permissions for Different Roles}
This system allows the manager to manage the permissions manually through provided interface. In the Role management table, the manager can decide what permissions a role is granted, which will be used in the schema file. 
    \begin{figure}[H]
    \centering
	    \includegraphics[scale=0.35]{Final Report/Implementation/image/FrontEndPermissionManage.png}
	\label{fig:my_label}
	\caption{Permission Table on Manager Side}
	\end{figure} 
    \begin{figure}[H]
    \centering
	    \includegraphics[scale=0.35]{Final Report/Implementation/image/UserPermission.png}
	\label{fig:my_label}
	\caption{Role Table on Manager Side}
	\end{figure} 
	In Schema file (\ref{Schema}), once a database is required, the system will firstly define whether the requesting user has permission to do corresponding operations.
    \begin{figure}[H]
	    \centering
	    \includegraphics[scale=0.55]{Final Report/Implementation/image/PermissionInSchema.png}
	\label{fig:my_label}
	\caption{Permission in Schema}
	\end{figure} 
\subsection{Import and Export Excel}
\subsubsection{Import Excel}\label{multiAccount}
Import excel function is placed in user management menu that is exclusive to the manager. This provides the manager a way to create multiple accounts through the excel file that contains a name list, along with pre-set password.\\
Initially, the user can upload the excel file in the format of `.xls', `.xlsx', `.csv'. Then a urlTobase64 function from front-end will be called to turn arraybuffer in files into base64 type. Then the base64 data will be transferred to a cloud function, in which the data becomes a record array and is convenient to submit.\\
Lastly, there will be a loop to call uni-id-cf function and create accounts. The procedure is the same as register.
    \begin{figure}[H]
    \centering
	    \includegraphics[scale=0.55]{Final Report/Implementation/image/transformTobase64.png}
	\label{fig:my_label}
	\end{figure}    
	\begin{figure}[H]
    \centering
	    \includegraphics[scale=0.35]{Final Report/Implementation/image/getFileAndParse.png}
	\label{fig:my_label}
	\caption{Upload File and Parse}
	\end{figure}
\subsubsection{Export Excel File}
An "Export Excel" functionality is provided in multiple interfaces, which can export the table content into excel tables.\\
To implement this function, a `downloadExcel' component is used. This component can be used by specifying its required data, such as field name, type and file name. Then the customized file will be exported and automatically downloaded.
    \begin{figure}[H]
    \subfigure[Export Excel Component]{
        \label{Fig.sub.1}
	    \includegraphics[scale=0.4]{Final Report/Implementation/image/loginCallFunction1.png}}
	 \hspace{2cm}
    \subfigure[Specified Properties of Exported File]{
        \label{Fig.sub.2}
	    \includegraphics[scale=0.45]{Final Report/Implementation/image/dataField.png}}
	\label{fig:my_label}
	\caption{Export Excel File}
	\end{figure}
\subsection{Upload and Store Image Data}
When creating questions for quizzes, teachers can upload images for questions.\\
To achieve this, uni-file-picker is used to let users select resources from the local directory. The file type is specified as image and the number limit is one. The uni-file-picker that accepts image will upload the image to server space and return a url of the image. To store the image in the database, the url is stored instead of the image file.
    \begin{figure}[H]
    \centering
	    \includegraphics[scale=0.55]{Final Report/Implementation/image/ImageFilePicker.png}
	\label{fig:my_label}
	\caption{uni-file-picker}
	\end{figure} 
    \begin{figure}[H]
    \centering
	    \includegraphics[scale=0.6]{Final Report/Implementation/image/imageUrl.png}
	\label{fig:my_label}
	\caption{Image url Storage}
	\end{figure}

